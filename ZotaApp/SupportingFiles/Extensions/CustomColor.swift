//
//  CustomColor.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/16/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
   static func redColor() -> UIColor {
        return UIColor.init(red: 190.0/255.0, green: 12.0/255.0, blue: 2.0/255.0, alpha: 1.0)
    }
    
}
