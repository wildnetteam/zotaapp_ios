//
//  DoubleExtension.swift
//  ZotaApp
//
//  Created by Azeem Ahmed on 8/25/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import Foundation


extension Double {
    /// Rounds the double to decimal places value
    func roundTo(_ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
