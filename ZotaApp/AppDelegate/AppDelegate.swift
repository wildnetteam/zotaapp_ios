//
//  AppDelegate.swift
//  ZotaApp
//
//  Created by Azeem Ahmed on 8/25/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        openCustomCamera()
        return true
    }
    
    func openCustomCamera(){
        let storyBoard : UIStoryboard = UIStoryboard(name:"CustomCamera", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
        
    }


}

